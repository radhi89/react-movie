import React from 'react';




const Pagination = (props) =>{

    const pageLinks =[]

    for(let i = 1; i <= props.pages +1 ; i++){

        let current = props.currentPage == i ? 'current':'';
        pageLinks.push(<li className ={`page-number ${current}`} key ={i} onClick={() =>props.nextPage(i)}>{i}</li>)

    }


    return(
      
      
       <div className="pagination">
            {props.currentPage > 1 ? <li className ='page-number prev' onClick={() =>props.nextPage(props.currentPage - 1 )}>Prev</li> : ''}
            {pageLinks}
            {props.currentPage < props.pages + 1 ? <li className ='page-number next' onClick={() =>props.nextPage(props.currentPage + 1)}>Next</li> : ''}
            </div>
           
    )
    

}





export default Pagination;