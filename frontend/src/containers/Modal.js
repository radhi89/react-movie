import React from 'react';


const Modal = ({ handleClose, show, children }) => {
 
  
    return (

      <div className={show ? "modal display-block" : "modal display-none"}>
        <section className="modal-main">
          <br></br>
          {children}
<br></br>
          <p><button onClick={handleClose}>Close</button></p>
        </section>
     
      </div>
    );
  };



  export default Modal;