import React from "react";
import {
  Link,
   } from 'react-router-dom';



function Premier(props) {
  if (props.item)
  {
    if (props.month == new Date(props.item.release_date).getMonth()+1) 
    {
      return(
      <div>          
            <li>
            <div className="date">{props.month}/19</div>
            <h2 className="entry-title"><Link to={`movie/${props.item.id}/review/`} >{props.item.title} <span className="rating" >Review : {props.item.review}</span></Link></h2>
           </li>
      </div>
            )
      } else 
      {
        return null
      }
  }
   
}
export default Premier;