import React, {Component} from 'react';


class DynamicSelect extends Component{
    constructor(props){
        super(props)
        this.state = { 
            selectedValue:'',
            selectedYear:'',
            
        }

        this.props.onSelectChange.bind(this)
       this.handleChange.bind(this)
        this.handleyearChange.bind(this)
       
       
    }

    //On the change event for the select box pass the selected value back to the parent
    handleChange = (event) =>
    {
        let selectedValue = event.target.value;
        let selectedYear = this.state.selectedYear
       
        
        this.props.onSelectChange(selectedValue,selectedYear);
        

       
        this.setState({
            selectedValue: selectedValue,
            selectedYear:selectedYear
        
        
        
        })
        
        {selectedValue !='Choose' && selectedYear !='Choose'? window.history.pushState(null, null, `/movie/?${selectedValue}&${selectedYear}`) :selectedYear =='Choose'? window.history.pushState(null, null, `/movie`): selectedYear =='Choose'&& selectedValue=="Choose" ? window.history.pushState(null, null, `/movie`):window.history.pushState(null, null, `/movie`)}
        {selectedValue =='Choose'&& selectedYear !='Choose'?window.history.pushState(null,null,`/movie/?&${selectedYear}`):selectedYear =='Choose'&& selectedValue !='Choose'? window.history.pushState(null,null,`/movie/?${selectedValue}&`):selectedValue =='Choose' && selectedYear =='Choose'? window.history.pushState(null, null, `/movie/`):window.history.pushState(null, null, `/movie/?${selectedValue}&${selectedYear}`)}
       
       
    }
    handleyearChange = (event) =>
    {
      

        let selectedYear= event.target.value;
        let selectedValue = this.state.selectedValue
       
        this.props.onSelectChange(selectedValue,selectedYear);
       
    
        this.setState({
            
            selectedYear:selectedYear,
            selectedValue:selectedValue
          
        })
        {selectedValue !='Choose' && selectedYear !='Choose'? window.history.pushState(null, null, `/movie/?${selectedValue}&${selectedYear}`) :selectedYear =='Choose'? window.history.pushState(null, null, `/movie`): selectedYear =='Choose'&& selectedValue=="Choose" ? window.history.pushState(null, null, `/movie`):window.history.pushState(null, null, `/movie`)}
        {selectedYear =='Choose'&& selectedValue !='Choose'? window.history.pushState(null,null,`/movie/?${selectedValue}&`):selectedValue =='Choose'&& selectedYear !='Choose'?window.history.pushState(null,null,`/movie/?&${selectedYear}`):selectedValue =='Choose' && selectedYear =='Choose'? window.history.pushState(null, null, `/movie/`):window.history.pushState(null, null, `/movie/?${selectedValue}&${selectedYear}`)}
        
       
       
    }


    render(){
        let arrayOfData = this.props.arrayOfData;
        let arrayOfyear = this.props.arrayOfyear;
        let options = arrayOfData.map((data) =>
        <option 
                    key={data.id}
                    value={data.name}
                > 
                   {data.name}
                </option>
            );
        let years = arrayOfyear.map((data) =>
      <option href
                key={data.id}
                value={data.name}
            > 
                {data.name}
            </option> 
        );

        
            return (
                <div>
         <select name="customSearch"  className="custom-search-select" onChange={this.handleChange}>
                <option>Choose</option>
                {options}
             </select>
            
            <select name="customSearch"  className="custom-search-select" onChange={this.handleyearChange}>
                <option>Choose</option>
                {years}
             </select>
             </div>
        
           
        )
    }
}

export default DynamicSelect;