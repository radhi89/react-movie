import React, { Component } from 'react';
import {Link,} from 'react-router-dom';
import StarRatings from 'react-star-ratings';



class MovieList extends Component {
  constructor(props) {
    super(props)
    this.state={
      currentPge :1,
      totalResults:0,
     }

  }
  
 render() 
 {
    const {movies} = this.props
    return (
      movies.length > 0 && movies.map((item, index) =>
       {
       return ( 
       
          <div key={index} className="movie">
            <div  key={index}>
              <figure className="movie-poster"><img src={item.banner_image} alt="#"></img></figure>
              <div className="movie-title"><Link to={`movie/${item.id}/review/`}>{item.title}</Link></div>
              <h3>Review : <StarRatings rating={item.review} starDimension="20px" starSpacing="2px" starRatedColor="yellow" changeRating={this.changeRating} numberOfStars={5} name='rating'/></h3>
             {/* <button><Link to={`movie/${item.id}/review/`} >Review</Link></button> */}
              <p>{item.description}</p>
            </div>
          </div>
               )
      })
        )
       
      }
    

}


export default MovieList;
