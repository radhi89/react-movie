import React, { Component } from 'react';
import MovieList from './MovieList';
import DynamicSelect from '../containers/DynamicSelect'
import Pagination from '../containers/Pagination';


const arrayOfData = [
	{
		id: '1 - Action',
		name: 'Action'
	},
	{
		id: '2 - Horror',
		name: 'Horror'
	},
	{
		id: '3 - Adventure',
		name: 'Adventure'
	},
	{
		id: '4 - Drama',
		name: 'Drama'
	},
	{
		id: '4 - Fantasy',
		name: 'Fantasy'
	},
];
const arrayOfyear = [
	{
		id: '1 - 2018',
		name: '2018'
	},
	{
		id: '2 - 2019',
		name: '2019'
	},
	{
		id: '3 - 2020',
		name: '2020'
	},



];


class MovieContainer extends Component {
	constructor(props) {
		super(props)

		this.state = {


			movies: [],
			totalResults: 0,
			currentPage: 1,
			nextPage: '',
			prevPage: '',
			selectedValue: '',
			selectedYear:'',
			



		}

	}


	componentDidMount() {
		this.fetchData();
		document.title = "Movie Masti:Movies"
	}


	fetchData() {

		fetch('https://movie-react-django.herokuapp.com/api/movies/').then((res) => {
			res.json().then(response => {
				this.setState({
					movies: response.results,
					currentPage: response.current, totalResults: response.count,
					nextPage: response.next, prevPage: response.previous,
					genre: response.results
				})
			})

		}).catch(err => {
			console.log('err', err)
		})



	}

	handleSelectChange = (selectedValue,selectedYear) => {
	

		if (selectedValue!=='Choose' && selectedYear !=='Choose'){
	
			fetch(`https://movie-react-django.herokuapp.com/api/movies/?year=${selectedYear}&genre=${selectedValue}`).then((res) => {
				res.json().then(response => {

					this.setState({
						selectedValue: this.state.selectedValue,
						selectedYear:this.state.selectedYear,
						movies: response.results,
						currentPage: response.current,
						totalResults: response.count, nextPage: response.next,
						prevPage: response.previous,
				
					});
				}).catch(err => {
					console.log('err', err)
				})
			}
			)


		}else if (selectedValue!=='Choose'){
			
			fetch(`https://movie-react-django.herokuapp.com/api/movies/?year=&genre=${selectedValue}`).then((res) => {
				res.json().then(response => {

					this.setState({
						selectedValue: this.state.selectedValue,
						selectedYear:this.state.selectedYear,
						movies: response.results,
						currentPage: response.current,
						totalResults: response.count, nextPage: response.next,
						prevPage: response.previous,
						
					});
				}).catch(err => {
					console.log('err', err)
				})
			}
			)
		}
				
		 else if(selectedYear!=='Choose'){
			
		
			fetch(`https://movie-react-django.herokuapp.com/api/movies/?year=${selectedYear}&genre=`).then((res) => {
				res.json().then(response => {
					this.setState({
						movies: response.results,
						selectedValue: this.state.selectedValue,
						selectedYear:this.state.selectedYear,
						currentPage: response.current,
						totalResults: response.count, nextPage: response.next,
						prevPage: response.previous,
						
					});
				}).catch(err => {
					console.log('err', err)
				})
			})
			

		}else{
			fetch(`https://movie-react-django.herokuapp.com/api/movies/`).then((res) => {
				res.json().then(response => {
					this.setState({
						movies: response.results,
						selectedValue: this.state.selectedValue,
						selectedYear:this.state.selectedYear,
						currentPage: response.current,
						totalResults: response.count, nextPage: response.next,
						prevPage: response.previous,
				
					});
				}).catch(err => {
					console.log('err', err)
				})
			})
			

		}
	
	}


	nextPage = (pageNumber) => {

		fetch(`https://movie-react-django.herokuapp.com/api/movies/?page=${pageNumber}`).then((res) => {
			res.json().then(response => {
				this.setState({
					movies: response.results,
					currentPage: response.current,
					totalResults: response.count, nextPage: response.next, prevPage: response.previous
				})
			})

		}).catch(err => {
			console.log('err', err)
		})


	}


	render() {

		const numberPages = Math.floor(this.state.totalResults / 8)


		return (
			<div>

				<main className="main-content">
					<div className="container">
						<div className="page">
							<div className="breadcrumbs">
								<a href="/">Home</a>
								<span>Movie</span>
							</div>
							<div className="filters">
<DynamicSelect selectedValue={this.state.selectedValue} selectedYear={this.state.selectedYear} arrayOfyear={arrayOfyear} arrayOfData={arrayOfData} onSelectChange={this.handleSelectChange} />
					
							</div>
						</div>

						<div className="movie-list">
							<MovieList movies={this.state.movies} />
							
						</div>
						
					</div>
					
						{this.state.totalResults > 8 ? <Pagination pages={numberPages} prevPage={this.nextPage} nextPage={this.nextPage} currentPage={this.state.currentPage} /> : ''}
					
				</main>
			</div>





		)
	}

}

export default MovieContainer;