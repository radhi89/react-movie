import React from "react";
import { Link, } from 'react-router-dom';


function Imagedisplay(props) {
  if (props.item) 
    {
      return (
        <div className={props.foot ? "col-sm-6 col-md-3" : "col-sm-6 col-md-12"}>
          <div className="latest-movie">
            <Link to={`movie/${props.item.id}/review/`} ><img src={props.item.banner_image} alt={props.item.title}></img></Link>
          </div>
        </div>
  
      )
    }
    else 
    {
      return null
    }
  
  }
  
  


export default Imagedisplay;