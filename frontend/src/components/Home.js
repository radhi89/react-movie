import React, {Component} from 'react';
import Premier from '../containers/Premier'
import Imagedisplay from './Imagedisplay'
import SimpleImageSlider from "react-simple-image-slider";
import MovieFooter from './MovieFooter';


class Home extends Component {
  constructor() {
    super()
    this.state = {
      movies : [],
      
    }
  }


  componentDidMount() {

    fetch('https://movie-react-django.herokuapp.com/api/movies/').then((res) => {
    res.json().then(response => {
          this.setState({ movies: response.results})
       
        })
    }).catch(err => {
      console.log('err', err)
      })
  }
  componentWillMount() {
    document.title = "Movie Masti:Home Page"
}

  render() {
    const images =  this.state.movies.length > 0 && this.state.movies.map((item) =>(
       ({url: item.banner_image })));

    const imagedisplay =  this.state.movies.length > 0 && this.state.movies.slice(0,4).map((item) =>(
      <Imagedisplay key={item.id} item={item} foot={true}/>
    ));
    const sideimagedisplay =  this.state.movies.length > 0 && this.state.movies.slice(0,2).map((item) =>(
      <Imagedisplay key={item.id} item={item} foot={false}/>
    ));
    const moviepremierdec =  this.state.movies.length > 0 && this.state.movies.map((item) =>(
      <Premier key={item.id} item={item} month="12"/>
    ));
    const moviepremiernov = this.state.movies.length > 0 && this.state.movies.map((item) =>(
      <Premier key={item.id} item={item} month="11"/>
    ));
    const moviepremieroct = this.state.movies.length > 0 && this.state.movies.map((item) =>(
      <Premier key={item.id} item={item} month="10"/>
       ));
  return (
    <div>
    <main className="main-content">
      <div className="container">
        <div className="page">
          <div className="row">
            <div className="col-md-9">
                   <SimpleImageSlider
                    width={780}
                    height={380}
                    images={images}
                />    
                  <br></br>
                  </div>
            <div className="col-md-3">
              <div className="row">
              {sideimagedisplay}
              </div>
            </div>
        </div>
        </div> 
            <div className="row">
              {imagedisplay}
            </div>
       
          <div className="row">
            <div className="col-md-4">
              <h2 className="section-title">December premiere</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
              <ul className="movie-schedule">
                {moviepremierdec}                            
              </ul> 
            </div>
            <div className="col-md-4">
              <h2 className="section-title">November premiere</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
              <ul className="movie-schedule">
              {moviepremiernov}
              </ul> 
            </div>
            <div className="col-md-4">
              <h2 className="section-title">October premiere</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
              <ul className="movie-schedule">
              {moviepremieroct}
              </ul>
            </div>
          </div>
        </div>
 
    </main>
          <MovieFooter />
  </div>


  )
                
            }
          }
          
  
export default Home;
