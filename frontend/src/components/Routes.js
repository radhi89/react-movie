import React, {Component} from 'react';
import Home from './Home';
import { Router, Link, Route, Switch } from 'react-router-dom';
import history from '../containers/history'
import Movies from './Movies';
import Review from './Review';
import Contact from './Contact';
import $ from 'jquery';



class Routes extends Component {
    constructor(props){
        super(props);

    this.state = {
        selectedIndex : 0
    }
      this.selectIndex = this.selectIndex.bind(this);
    }

    componentDidMount =() =>{
   
        $(".mobile-navigation").append($(".main-navigation .menu").clone());
  }
    navigate=()=>{
       
        $('.mobile-navigation').slideToggle();

    }

    selectIndex = (index)=>{
        this.setState({selectedIndex: index}, () => {
       
        })
    }


    render() {
        const selectedIndex = this.state.selectedIndex
   
      return (
         
        <Router history={history}>
            <div id="site-content">
                <header className="site-header">
                    <div className="container">
                        <a href="/" id="branding">
                        <img src="/images/logo.png" alt="" className="logo"></img>
                        <div className="logo-copy">
                            <h1 className="site-title">MOVIE MASTI</h1>
                            <small className="site-description">Movie Rating..</small>
                        </div>
                        </a> 
                         
                        <div className="main-navigation">
                            <button type="button" onClick={() => this.navigate()} className="menu-toggle"><i className="fa fa-bars"></i></button>
                            <ul className="menu">
                                <li className={selectedIndex == 0 ? 'menu-item current-menu-item' : 'menu-item'} onClick={()=> this.selectIndex(0)}><Link to="/">Home</Link></li>
                                      
                                <li className={selectedIndex == 1 ? 'menu-item current-menu-item' : 'menu-item'} onClick={()=> this.selectIndex(1)}><Link to="/movie">Movie</Link></li>
                                    
                                <li className={selectedIndex == 2 ? 'menu-item current-menu-item' : 'menu-item'} onClick={()=> this.selectIndex(2)}><Link to="/contact">Contact Us</Link></li>
                                     
                            </ul>
                                    
                        </div>
                         
                        <div className="mobile-navigation"></div>
                    </div>
                </header>
                
           <Switch>
                            
            <Route exact={true} path="/" component={Home}/>
            <Route exact={true} path="/movie" component={Movies}/>
             <Route exact path="/movie/:id/review/" component={Review} />
            <Route exact={true} path="/contact" component={Contact}/>
                      
        </Switch> 
            </div>
        </Router>
           );
        }
        
      }    
     

export default Routes;