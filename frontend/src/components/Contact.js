import React, {Component}  from 'react';
import MapContainer from './MapContainer';
import MovieFooter from './MovieFooter';
import Modal from '../containers/Modal'



class Contact extends Component {
    constructor(props){
        super(props);
        this.successMessageTimeoutHandle = 0;
        this.submitForm=this.submitForm.bind(this);
        this.postedjob=this.postedjob.bind(this);
        this.state={
            name:"",
            email:"",
            content:"",
            successMessage:'',
            show: false
            
        }

 
    }

    submitForm= (evt) =>{
        evt.preventDefault()
        const form = {
            name: this.state.name,
            email: this.state.email,
            content: this.state.content,
            
        };
    
        this.postedjob(form)
        this.validateEmail(form.email) 
        this.setSuccessMessage('Thank you for your message')
        this.showModal()

      };


    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@/"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        return re.test(email);
      }
      

      showModal = () => {
        this.setState({ show: true });
      };
    
      hideModal = () => {
        this.setState({ show: false });
      };
     



    postedjob = (payload)=>{
    
    
        fetch('https://movie-react-django.herokuapp.com/api/contact/',
            {
                method: 'POST',
                body: JSON.stringify(payload),
                headers: {
                    'Content-Type': 'application/json'
                        }
               
                
            }
        ).then(response =>response.json()
        )
       
        };


 setSuccessMessage(message) {
        clearTimeout(this.successMessageTimeoutHandle);
        this.successMessageTimeoutHandle = setTimeout(() => {
            this.setState({
                successMessage: '',
                name:"",
                email:"",
                content:"",
            });
               })
          
          
                
    
    }
    componentWillMount() {
        document.title = "Movie Masti:Contact Us"
    }

    render() {
    
        return(
            <div>
                <main className="main-content">
                        <div className="container">
                            <div className="page">
                                <div className="breadcrumbs">
                                    <a href="/">Home</a>
                                    <span>Contact Us</span>
                                </div>
        
                                <div className="content">
                                    <div className="row">
                                  
                                        <div className="col-md-4">
                                         
                                            <ul className="contact-detail">
                                                <li>
                                                    <img src="/images/icon-contact-map.png" alt="#"></img>
                                                    <address><span>Movie Masti. INC 550 Avenue Street, New york</span></address>
                                                </li>
                                                <li>
                                                    <img src="/images/icon-contact-phone.png" alt=""></img>
                                                    <a href="tel:1590912831">+1 590 912 831</a>
                                                </li>
                                                <li>
                                                    <img src="/images/icon-contact-message.png" alt=""></img>
                                                    <a href="mailto:contact@companyname.com">movie-contact@companyname.com</a>
                                                </li>
                                            </ul>
                                      
                <div className="contact-form">
                    <h1>{this.state.currenttask}</h1>
                <form onSubmit={this.submitForm}>
    
                    <input type='text'  required placeholder="name..." value={this.state.name} onChange={e=>this.setState({name:e.target.value})}/>
                 
    
                    <input type='email' required placeholder="email..." value={this.state.email} onChange={e=>this.setState({email:e.target.value})}/>
                  
                    <input type='text' required placeholder="message..." value={this.state.content} onChange={e=>this.setState({content:e.target.value})}/>
                    
                    <button type="submit" >Submit</button>
    
              </form>
              </div>
              <Modal show={this.state.show} handleClose={this.hideModal}>
           
          <h2>Thank you for your message</h2>
    

        </Modal>
    
               </div>
                    <div className="col-md-7 col-md-offset-1">
                        <div className="map">
                            <MapContainer />
                        </div>
                                            

                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                    
                    <MovieFooter />

                </div>
               
        )
    }
}

  
export default Contact;