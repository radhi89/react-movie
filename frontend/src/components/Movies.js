import React, {Component}  from 'react';
import MovieContainer from '../moviepage/MovieContainer';
import MovieFooter from '../components/MovieFooter';


class Movies extends Component {
  
    

  render() {
    return (
      <div>
        <MovieContainer />
        <MovieFooter />
      </div>
  );
}

 
}


export default Movies;