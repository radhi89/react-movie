import React, {Component}  from 'react';
import StarRatings from 'react-star-ratings';



class Review extends Component {
    constructor(props){
        super(props);
        this.successMessageTimeoutHandle = 0;
        this.postedjob=this.postedjob.bind(this);
        this.changeRating=this.changeRating.bind(this);
        this.state={
            review: null,
            movie: null,
            movies:[],
            successMessage:''
          } 
    }

    componentDidMount() {
        document.title = "Movie Masti:Review"
        const { params: { id } } = this.props.match; 
        fetch(`https://movie-react-django.herokuapp.com/api/movies/${id}/`).then((res) => {
    
    
            res.json().then(response => {
            this.setState({ movies: response , movie:id})
          })
  
        }).catch(err => {
          console.log('err', err)
        })

      }
    
    changeRating=( newRating, name) =>{
        const form={
            movie: this.state.movies.id,
           review: newRating
            
        };
        this.postedjob(form)
        this.setState({
            rating: newRating
          });
        this.setSuccessMessage('Thank you for your time, your review is loading...')
      
        
           
           
          
            
        
      }

     
    postedjob = (payload)=>{
    
    
        fetch(`https://movie-react-django.herokuapp.com/api/review/`,
            {
                method: 'POST',
                body: JSON.stringify(payload),
                headers: {
                    'Content-Type': 'application/json'
                }
               
                
            }
        ).then(response => response.json())
     
        
    };

    setSuccessMessage(message) {
        clearTimeout(this.successMessageTimeoutHandle);
        this.successMessageTimeoutHandle = setTimeout(() => {
           
                this.setState({
                    successMessage: message,
                  
                });  
               
            })
                setTimeout(() => {
                    
                    this.setState({
                        successMessage: ''
                    });
                   
                    this.successMessageTimeoutHandle = 0;
                    window.location.reload()
                }, 3000)
          
                
    
    }
    render() {
        return (

        <div>
       
               <main className="main-content">
                    <div className="container">
                        <div className="page">
                        <div className="breadcrumbs">
							<a href="/">Home</a>
							<span>Movie:Review</span>
						</div>

						
                            <div className="content">
                                <div className="row">
                                {this.state.successMessage !='' ? <div className="alert alert-success text-center">
                    {this.state.successMessage}
                </div> : '' }
                                    <div className="col-md-6">
                                        <figure className="movie-poster"><img src={this.state.movies.banner_image} alt="#"></img></figure>
                                    </div>
                                    <div className="col-md-6">
                                        <h2 className="movie-title">{this.state.movies.title}</h2>
                                        
                                        <div className="movie-summary">
                                            <p>{this.state.movies.description}.</p>
                                        </div>
                                        <ul className="movie-meta">
                                            <li><strong>Rating:</strong> 
                                            {this.state.currenttask}
                                            <StarRatings rating={this.state.movies.review} starRatedColor="yellow" changeRating={this.changeRating} numberOfStars={5} name='rating'/> 
                                            </li>
                                            <li><strong>Length:</strong> {this.state.movies.duration}</li>
                                            <li><strong>Premiere:</strong> {this.state.movies.release_date}</li>
                                            <li><strong>Category:</strong>{this.state.movies.genre}</li>
                                        </ul>
    
                                        <ul className="starring">
                                            <li><strong>Directors:</strong> {this.state.movies.director}</li>
                                            <li><strong>Writers:</strong> Chris Sanders (screenplay), Kirk De Micco (screenplay)</li>
                                            <li><strong>Stars:</strong> {this.state.movies.stars}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="entry-content">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac pharetra libero. Integer in suscipit diam, sit amet eleifend nunc. Curabitur egestas nunc nulla, in aliquet risus efficitur quis. Vivamus facilisis est libero, vitae iaculis nulla cursus in. Suspendisse potenti. In et fringilla ipsum, quis varius quam. Morbi eleifend venenatis diam finibus vehicula. Suspendisse eu blandit metus. Sed feugiat pellentesque turpis, in lacinia ipsum. Vivamus nec luctus orci.</p>
                                    <p>Aenean vehicula eget risus sit amet posuere. Maecenas id risus maximus, malesuada leo eget, pellentesque arcu. Phasellus vitae leo rhoncus, consectetur mauris vitae, lacinia quam. Nunc turpis erat, accumsan eget justo quis, auctor ultricies magna. Mauris sodales, risus sit amet hendrerit tincidunt, erat ante facilisis sapien, sit amet maximus neque massa a felis. Nullam consectetur justo massa, vel commodo metus gravida in. Aliquam erat volutpat. Nullam a lorem sed lorem euismod gravida a eu velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec venenatis ac ligula vel pharetra. Aenean vitae nulla sed dui volutpat euismod. Nam ex quam, consequat id rutrum sed, porttitor id lectus. Vestibulum venenatis consectetur justo ut sagittis. Duis dignissim tincidunt quam, nec pulvinar libero luctus nec. Morbi blandit nec lorem in ullamcorper.</p>
                                    <p>Vestibulum et odio massa. Integer at odio ipsum. Proin vitae tristique nibh. Aenean semper ante sit amet ante ultricies tincidunt. Curabitur cursus, urna non ultricies posuere, dolor lacus cursus lorem, a dapibus nibh ex eget sem. Aliquam semper sagittis sapien a fermentum. Nullam sed iaculis lacus, et imperdiet risus. Praesent quis turpis ac nunc sodales tincidunt. Aliquam at leo odio. Sed a tempor nisl, et mattis felis. Nam mauris nunc, commodo ac orci ut, auctor viverra mauris.</p>
                                    <p>Quisque nec justo vitae metus consectetur ultrices. Duis venenatis lorem massa, eu pulvinar quam faucibus sed. Nulla fringilla lorem sit amet sagittis mattis. Nunc in leo a odio mollis consectetur. Etiam ac nisl eget diam ullamcorper porta. Aliquam consectetur neque eget metus egestas sollicitudin. Curabitur ultrices urna et feugiat malesuada.</p>
                                    <p>Nulla facilisi. Fusce sed dapibus leo, eu lobortis ante. Duis luctus mauris in ante semper, ut feugiat nisi condimentum. Nullam a odio et justo suscipit tempus. Vestibulum placerat dapibus quam, a egestas turpis efficitur id. Integer suscipit placerat placerat. Phasellus in lorem quis leo egestas accumsan. Nam et euismod ligula. Duis nec erat aliquam, sollicitudin diam non, ornare leo. Pellentesque augue leo, faucibus in nunc nec, tincidunt ullamcorper tortor. Phasellus aliquam condimentum elit. Nulla facilisi. Donec magna libero, bibendum eu faucibus et, mattis at felis. Integer turpis nibh, blandit nec elit vel, euismod laoreet quam. Donec vel ante nisi. Nunc luctus a tellus non.</p>
                                </div>
                            </div>
                        </div>
                     
          
                     <h1></h1>
     
                        </div>
                </main>
            </div>
        )
        
    }
  
}

export default Review;