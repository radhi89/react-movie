
import React, {Component}  from 'react';


class MovieFooter extends Component {
  
render(){
    return(
      <div>
            <footer className="site-footer">
              <div className="container">
                  <div className="row">
                      <div className="col-md-2">
                          <div className="widget">
                              <h3 className="widget-title">About Us</h3>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia tempore vitae mollitia nesciunt saepe cupiditate</p>
                          </div>
                      </div>
                      <div className="col-md-2">
                          <div className="widget">
                              <h3 className="widget-title">Recent Review</h3>
                              <ul className="no-bullet">
                                  <li>Lorem ipsum dolor</li>
                                  <li>Sit amet consecture</li>
                                  <li>Dolorem respequem</li>
                                  <li>Invenore veritae</li>
                              </ul>
                          </div>
                      </div>
                      <div className="col-md-2">
                          <div className="widget">
                              <h3 className="widget-title">Help Center</h3>
                              <ul className="no-bullet">
                                  <li>Lorem ipsum dolor</li>
                                  <li>Sit amet consecture</li>
                                  <li>Dolorem respequem</li>
                                  <li>Invenore veritae</li>
                              </ul>
                          </div>
                      </div>
                      <div className="col-md-2">
                          <div className="widget">
                              <h3 className="widget-title">Join Us</h3>
                              <ul className="no-bullet">
                                  <li>Lorem ipsum dolor</li>
                                  <li>Sit amet consecture</li>
                                  <li>Dolorem respequem</li>
                                  <li>Invenore veritae</li>
                              </ul>
                          </div>
                      </div>
                      <div className="col-md-2">
                          <div className="widget">
                              <h3 className="widget-title">Social Media</h3>
                              <ul className="no-bullet">
                                  <li>Facebook</li>
                                  <li>Twitter</li>
                                  <li>Google+</li>
                                  <li>Pinterest</li>
                              </ul>
                          </div>
                      </div>
                      <div className="col-md-2">
                          <div className="widget">
                              <h3 className="widget-title">Newsletter</h3>
                              <form action="#" className="subscribe-form">
                                  <input type="text" placeholder="Email Address"></input>
                              </form>
                          </div>
                      </div>
                  </div> 

         <div className="colophon">Copyright 2020 Company name, Designed by Radhika. All rights reserved</div>
        <script src="./static/js/jquery-1.11.1.min.js"></script>
      <script async defer src="./static/js/plugins.js"></script>
      <script async defer src="./static/js/app.js"></script>
 
      </div> 

          </footer>
          
    </div>
   
 )
   
  
    } 
}
    
export default MovieFooter;