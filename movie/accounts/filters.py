import rest_framework_filters as filters

from accounts.models import Movie


class MovieFilter(filters.FilterSet):

    class Meta:
        model = Movie
        fields = ['genre', 'release_date']

    # def filter_movie(self, qs, name, value):
    #     movie_list = Movie.objects.filter(
    #         genre=self.genre).values_list('review', flat=True)
    #     qs = qs.filter(id__in=movie_list)
    #     return qs
    #
    # def filter_year(self, qs , value):
    #     movie_list = Movie.objects.filter(
    #         release_date__year=value).values_list('movie', flat=True)
    #     return  movie_list