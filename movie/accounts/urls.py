
from django.urls import path

from . import views

urlpatterns = [
    path('', views.HomeView.as_view()),
    path('contact/', views.EnquiryCreateViewSet.as_view()),
    path('movies/', views.MovieReviewViewSet.as_view()),
    path('review/', views.MovieReviewDetailViewSet.as_view()),
    path('movies/<int:id>/', views.MovieDetailCustom.as_view()),

]
